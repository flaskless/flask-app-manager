import os

from setuptools import find_packages, setup
# [[[cog
# p(f'''
# NAME='{values.project.flask_app}'
# VERSION='{values.project.version}'
# ''')
# ]]]

NAME='flask_app_manager'
VERSION='1.0'
# [[[end]]]

def package_files(directory):
    paths = []
    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            paths.append(os.path.join("..", path, filename))
    return paths

requirements = [
    'flask'
]

dev_requirements = ["isort", "black"]
test_requirements = ["pytest"]

docs_requirements = [
    "mkdocs",
    "pymdown-extensions",
    "mkdocs-material",
    "mkdocstrings",
    "mkdocs-macros-plugin",
] + dev_requirements

main_requirements = [
]

packages = find_packages(exclude=["tests.*", "tests", "requirements", "deploy","docs","app_manager_example"])

setup(
    name=NAME,
    version=VERSION,
    package_data={NAME: package_files(NAME)},
    entry_points={'console_scripts': [f'{NAME}={NAME}:cli']},
    python_requires=">3.8.0",
    packages=packages,
    extras_require={
        "test": requirements
                + test_requirements,
        "docs": requirements
                + docs_requirements,
        "main": requirements
                + main_requirements,
        "all":  requirements
                + main_requirements
                + test_requirements
                + docs_requirements
                + dev_requirements,
    },
    include_package_data=True,
    install_requires=requirements,
    tests_require=test_requirements,
    test_suite="tests",
)
