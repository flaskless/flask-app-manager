import flask
import fs
import os
class FlaskFS:

    def __init__(self,app=None) -> None:
        super().__init__()

        self._fs = None
        self._load_fs = load_fs
        if app:
            self.init_app(app)

    def _default_root(self,app=None):
        if app is None:
            app = flask.current_app
        r = app.instance_path 
        if not r:
             r = f'/tmp'
        r = os.path.join(r,app.name)
        return r

    def init_app(self,app:flask.Flask):
        default_root = self._default_root(app)
        app.config.setdefault('FLASK_FS_ROOT',default_root)

        app.extensions['fs'] = self

    @property
    def fs(self):

        if self._fs is None:
            uri = flask.current_app.config.get('FLASK_FS_ROOT')
            self._fs = self._load_fs(uri)

        return self._fs

    def fs_loader(self,f):
        """Decorator to load fs"""
        self._load_fs = f
        return f

from werkzeug.local import LocalProxy

def load_fs(uri):
    root_fs = fs.open_fs(uri+'/apps',writeable=True,create=True,)

    from fs.mountfs import MountFS
    from fs.tempfs import TempFS
    tmp_fs = TempFS()
    combined_fs = MountFS()
    combined_fs.mount('apps', root_fs)
    combined_fs.mount('tmp', tmp_fs)

    return combined_fs


def _current_fs_loader():
    if 'fs' not in flask.current_app.extensions:
        raise Exception('flask-fs extention not configured')

    return flask.current_app.extensions['fs'].fs 

current_fs = LocalProxy(_current_fs_loader)
