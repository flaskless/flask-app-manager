import flask
from werkzeug.local import LocalProxy
import functools

APP_MANAGER_URL_PREFIX_DEFAULT ='/apps'
""" Default url prefix for the app manager blueprint. Set to None to disable blueprint"""
APP_MANAGER_APPS_PATH ='apps'
""" Path to search for applications. Relative paths will be relative to the os.path.join(app.instance_path,app.name)"""


class ApplicationManager:

    def __init__(self,app=None,session_key='_app') -> None:
        self._after_create_app_funcs = []
        self._before_shutdown_app_funcs = []
        self._app_loader = None
        self._session_key = session_key

        if app is not None:
            self.init_app(app)

    def after_create_app(self, f):
        """
        Add methods to be called after the application is initialized
        """
        self._after_create_app_funcs.append(f)
        return f

    def before_shutdown_app(self,f):
        """
        Add methods to be called after the application is initialized
        """
        self._before_shutdown_app_funcs.append(f)
        return f
    
    def init_app(self, app: flask.Flask, app_loader=None):

        @app.context_processor
        def applications_context():
            current_application = None
            if flask.has_request_context():
                current_application = self.current_application
            return dict(
                applications=self,
                current_application=current_application
                )

        app.extensions["app_manager"] = self

        url_prefix = app.config.get('APP_MANAGER_URL_PREFIX',APP_MANAGER_URL_PREFIX_DEFAULT)

        if url_prefix is not None:
            from .views import bp
            app.register_blueprint(bp,url_prefix=url_prefix)

    @property
    def current_application(self):
        return self._load_app_from_session()

    def url_for(self,path,**kwargs):
        from .views import BLUEPRINT_NAME
        return flask.url_for(f'{BLUEPRINT_NAME}.launch',path=path,**kwargs)
        
    def launch_app(self,path,prefix=None):
        return self._launch_app(path)

    def restart_app(self,path=None):
        path = path or self.current_application
        self.remove_app(path)
        return self._restart_app(path)

    def shutdown_app(self,path=None):
        path = path or self.current_application
        self.remove_app(path)
        return self._shutdown_app(path)

    def fetch_app(self,app_uri):
        """Fetch the app at the profived uri and return the local path
            Override this function to customize how the app is loaded
        """
        import flask, sys,os

        if 'fs' in flask.current_app.extensions:
            import os
            current_fs = flask.current_app.extensions['fs'].fs
        
            path = app_uri
            fs_path = os.path.join('apps',path)
            syspath = current_fs.getsyspath(fs_path)
            if flask.current_app.config.get('APP_MANAGER_USE_TEMP',False):
                current_fs.makedirs(f'/tmp/{path}',recreate=True)
                current_fs.copydir(fs_path,f'/tmp/{path}',create=True)
                syspath = current_fs.getsyspath(f'/tmp/{path}')

            return syspath

        elif not os.path.isabs(app_uri):

            apps_path = flask.current_app.config.get('APP_MANAGER_APPS_PATH',APP_MANAGER_APPS_PATH)
            if not os.path.isabs(apps_path):
                apps_path = os.path.join(flask.current_app.instance_path,apps_path)    
            app_uri = os.path.join(apps_path,app_uri)

        return app_uri

    @functools.lru_cache(maxsize=128)
    def load_app(self, app_uri,skip_prepare=False):
        """
        Return a flask app from a FLASK_APP specification.
        """
        import flask.cli
        import os
        from urllib.parse import urlparse, parse_qsl
        app_import_path = self.fetch_app(app_uri)
        parsed = urlparse(app_import_path)
        query_params = dict(parse_qsl(parsed.query))
        app_import_path = parsed.path
        script_info = flask.cli.ScriptInfo(app_import_path=app_import_path)

        script_info.data = flask.session.get('launch-args',{})
        
        script_info.data.update(query_params)

        try:
            flask_app = script_info.load_app()
        except Exception:
            self.shutdown_app()
            raise

        if not skip_prepare:
            self.prepare_app(flask_app)

        return flask_app

    def prepare_app(self,app):
        flask_app = app
        for f in self._after_create_app_funcs:
            flask_app = f(flask_app) or flask_app

        return flask_app

    def remove_app(self,path):
        for f in self._before_shutdown_app_funcs:
            f(path)
            
    def _launch_app(self, path):
        """
        Sets the current application.  By default the application is stored in the session
        """
        
        flask.session[self._session_key] = path
        flask.session["_restart"] = False

    def _shutdown_app(self, path):
        """
        Sets the current application.  By default the application is stored in the session
        """
        if self._session_key in flask.session: 
            del flask.session[self._session_key]
            self.load_app.cache_clear()

    def _restart_app(self, path):
        self.shutdown_app(path)
        self.launch_app(path)
        flask.session["_restart"] = True

    def _should_restart(self):
        return flask.session.pop('_restart',False)

    def _load_app_from_session(self):
        """
        Get the current application from the session
        """
        return flask.session.get(self._session_key)

current_app_manager = LocalProxy(lambda: flask.current_app.extensions.get("app_manager"))
""" global accessor for the current ApplicationManager """
