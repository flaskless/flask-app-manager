import flask
import adminlte_base
import flask_adminlte_full
import flask_app_manager
import os



app = flask.Flask('flask-app-manager-example'
                    ,template_folder=os.path.join(os.path.dirname(__file__),'templates')
                    ,static_url_path='/apps/static')

try:
    from .fs import FlaskFS, current_fs
    flask_fs = FlaskFS(app)
except:
    flask_fs = None

adminlte = flask_adminlte_full.AdminLTE(app)

@adminlte.manager.menu_loader
class AdminMenuLoader(adminlte_base.MenuLoader):
    def navbar_menu(self):
        return []
    def sidebar_menu(self):
        return []
    def second_sidebar_menu(self):
        return []

app_manager = flask_app_manager.ApplicationManager(app)


class InstalledApp:
    def __init__(self, icon, name, path) -> None:
        self.icon = icon
        self.name = name
        self._path = path

    @property
    def path(self):
        return flask.url_for("applications.launch", path=self._path)


apps = [
    InstalledApp(icon="fas fa-blog", name=f"Blog Example", path="bluelog/wsgi.py"),
    InstalledApp(icon="fas fa-palette", name=f"Theme Docs", path="flask-adminlte-full/example"),
    InstalledApp(icon="fas fa-sticky-note", name="SayHello", path="sayhello/sayhello"),
    InstalledApp(icon="fas fa-pen", name="Editor", path="flaskcode/app.py"),
    InstalledApp(icon="fas fa-page", name="Content", path="akamatsu/akamatsu:init_app"),
]


@app_manager.load_apps
def load_apps():
    return apps

@app_manager.after_create_app
def set_instance_path(app: flask.Flask):
    import os
    if flask.current_app.env == 'serverless':
        app.instance_path = os.path.join('/tmp',app.name,'instance')

    os.makedirs(app.instance_path, exist_ok=True)

@app_manager.after_create_app
def initialize_db(app: flask.Flask):
    if "sqlalchemy" not in app.extensions:
        return app

    db = app.extensions["sqlalchemy"].db

    app.config[
        "SQLALCHEMY_DATABASE_URI"
    ] = f"sqlite:////{app.instance_path}/{app.name}.sqlite"

    with app.app_context():
        db.create_all()
    return app


@app_manager.after_create_app
def enable_debug_toolbar(app: flask.Flask):

    if not flask.current_app.config.get("DEBUG_TB_ENABLED", True):
        return

    app.config["DEBUG_TB_TEMPLATE_EDITOR_ENABLED"] = True

    if app.config.get("DEBUG_TB_ENABLED", True):
        return

    try:
        from flask_debugtoolbar import DebugToolbarExtension
    except Exception as e:
        print(f'Ignoring flask-debugtoolbar {e}')
        return
    toolbar = DebugToolbarExtension()
    toolbar.init_app(app)

app.secret_key = 'dummysecretckey'