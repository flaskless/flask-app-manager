from flask import Flask
from threading import Lock
from werkzeug.middleware.dispatcher import DispatcherMiddleware
from werkzeug.exceptions import NotFound

DEFAULT_GATEWAY_URL = '/gateway'
DEFAULT_GATEWAY_NAME = 'gateway'

class PrefixMiddleware(object):
    """
    Middleware to add prefix to all routes of an app
    https://stackoverflow.com/a/33320934
    """
    def __init__(self, app, prefix=''):
        self.app = app
        self.script_name = prefix

    def __call__(self, environ, start_response):
        script_name = self.script_name
        if self.script_name:
            environ['SCRIPT_NAME'] = script_name
            path_info = environ['PATH_INFO']
            if path_info.startswith(script_name):
                environ['PATH_INFO'] = path_info[len(script_name):]

        return self.app(environ, start_response)


class ApplicationGateway(DispatcherMiddleware):
    """
    A WSGI dispatcher for mounting apps to routes at runtime
    """
    def __init__(self, app: Flask = None ,url_prefix=DEFAULT_GATEWAY_URL,name=DEFAULT_GATEWAY_NAME):        
        super(ApplicationGateway,self).__init__(NotFound())
        self._before_request_func = lambda:None
        self._lock = Lock()
        self.name = name
        self.flask_app = None
        if app:
            self.init_app(app,url_prefix=url_prefix,name=name)
    
    def init_app(self,app,url_prefix=None,name=None):


        url_prefix = url_prefix or app.config.get('GATEWAY_URL',DEFAULT_GATEWAY_URL)
        name = name or app.config.get('GATEWAY_NAME',DEFAULT_GATEWAY_NAME)

        self.app = app.wsgi_app

        self.flask_app = app
        self.name = name
       # self.mount_app(app)
        self.mount_app(app,url_prefix)
        app.wsgi_app = self 

        def mount_app(subapp,prefix=''):
            self.mount_app(subapp,prefix)
            subapp.wsgi_app = self

        app.extensions[self.name] = self
        app.mount_app = mount_app

    def mount_app(self,app,url_prefix=''):

        if app == None or app==False:
            self.unmount(url_prefix=url_prefix)
            return

        if not callable(app):
            raise RuntimeError('App must be callable functions')
        
        if url_prefix == '/':
            url_prefix = ''

        wsgi_app = app.wsgi_app if isinstance(app,Flask) else app

        with self._lock:
            self.mounts.update({url_prefix:wsgi_app})
            
    def unmount(self,url_prefix=''):
        if url_prefix in self.mounts: 
            del self.mounts[url_prefix]
            return True
        return False

    def before_request(self,f):
        self._before_request_func = f
        return f

    @property
    def wsgi_app(self):
        return super(ApplicationGateway,self).__call__

    def __call__(self, environ, start_response):
        rv = self.wsgi_app
        with self.flask_app.request_context(environ):
            try:
                rv = self._before_request_func() or rv
            except Exception as e:
                # Handle error as if it came from original flask app
                rv = self.flask_app.handle_exception(e)
        
        response = rv(environ,start_response)

        return response
