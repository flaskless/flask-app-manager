

import flask

from .app_manager import current_app_manager

LAUNCH_PREFIX = ''
BLUEPRINT_NAME='applications'

bp = flask.Blueprint(BLUEPRINT_NAME, __name__, template_folder="templates")

# TODO:
# rewrite this using openapi + actions after the OpenAPI + codegen editor is integrated

@bp.route(f"{LAUNCH_PREFIX}/",defaults={'path':None,'id':None})
@bp.route(f"{LAUNCH_PREFIX}/<path:path>")
@bp.route(f"{LAUNCH_PREFIX}/<int:id>")
def launch(path=None,id=None):
    prefix = flask.request.args.get('prefix')
    restart = flask.request.args.get('restart')
    delete = flask.request.args.get('shutdown')
    next = flask.request.args.get('next')
        
    flask.session.pop('launch-args',None)
    config = flask.request.args.to_dict()

    #config = {k:v for (k,v) in config if k==k.upper()}
    if config:
        if 'referrer' not in config:
            config['referrer'] = flask.request.referrer
            
        flask.session['launch-args'] = config
        restart = True

    path = path or current_app_manager.current_application

    if restart is not None:
        current_app_manager.restart_app(path)

    if delete is not None:
        current_app_manager.shutdown_app(path)

    else:    
        current_app_manager.launch_app(path,prefix=prefix)

    prefix = flask.request.url_root + prefix if prefix else None

    return flask.redirect(next or prefix or flask.request.host_url)