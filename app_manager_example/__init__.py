
if __name__ == '__main__':
    from flask import current_app, Flask
    from flask_login import current_user
    from flask_app_manager import ApplicationGateway
    gateway_app = Flask('gateway')

    gateway = ApplicationGateway(gateway_app)
    @gateway.before_request
    def check_auth():
        if not current_user.is_authenticated:
            return current_app.login_manager.unauthenticated()
    
    app = Flask('simpleapp')
    @app.route('/')
    def hello():
        return 'hello from simpleapp'

    gateway.mount_app(app,'')
    
    app.run()